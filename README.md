# SkyCast Readme

## [See app in action](https://lit-island-18559.herokuapp.com/)

A weather web application created with Django. Fetches weather data from all over the world using the DarkSky API and the GoogleMaps API.

Background taken from Pixabay.com.

Niko Tsirakis <niko.tsirakis@gmail.com>
