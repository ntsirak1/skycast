from django.apps import AppConfig


class SkycastConfig(AppConfig):
    name = 'SkyCast'
