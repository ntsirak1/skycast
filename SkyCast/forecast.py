from googlemaps import Client as GClient

from forecastiopy.FIOCurrently import FIOCurrently as FCurrent
from forecastiopy.ForecastIO import ForecastIO as Forecast

# TODO: move these elsewhere and load them in at runtime
GOOGLE_MAPS_API_KEY = 'AIzaSyC7pbbYYvaJ6rZkbq1pCGvbmzsTA0_3TOc'
FORECAST_API_KEY = 'b96228812dc34dc9a82e18a331ef02ac'

DEGREE_UNICODE_SYMBOL = u'\xb0'

class ForecastWrapper:

    '''
    Wrapper around forecast python API.
    Used mainly for simplicity.
    '''

    def __init__(self, location_str):
        self._lat = self._lng = self._full_location = None

        self._get_location(location_str)
        self._weather = self._fetch_weather()

    # Use Google Maps Geocoding API to convert location string
    # to a pair of latitude and longitude coordinates
    def _get_location(self, location_str):
        g = GClient(key=GOOGLE_MAPS_API_KEY)
        geocode = g.geocode(location_str)

        if geocode[0]:
            self._lat = geocode[0]['geometry']['location']['lat']
            self._lng = geocode[0]['geometry']['location']['lng']
            self._full_location = geocode[0]['formatted_address']

    # Use DarkSky forecast API to fetch weather data for specified location
    def _fetch_weather(self):
        weather = dict()

        if self._lat and self._lng:
            f = Forecast(apikey=FORECAST_API_KEY,
                         latitude=self._lat,
                         longitude=self._lng)

            if f.has_currently():
                current = FCurrent(f)
                for item in current.get().keys():
                    weather[item] = str(current.get()[item])

                return weather

        return None

    # Return the full weather dictionary
    def get_weather(self):
        return self._weather

    def get_location(self):
        return self._full_location

    # Get temperature string with degree symbol in specified unit
    def get_temperature_str(self, unit='F'):

        temp = int(float(self._weather['temperature']))

        if unit == 'C':
            temp = ((temp - 32) * 5) / 9

        return '{}{}{}'.format(temp, DEGREE_UNICODE_SYMBOL, unit)

    def get_humidity_str(self):
        return '{}%'.format(float(self._weather['humidity'])*100)

    def get_precipitation_str(self):
        return '{}%'.format(float(self._weather['precipProbability'])*100)

    def get_summary(self):
        return self._weather['summary']
