from django.shortcuts import render, render_to_response
from django.template import Engine, Template, Context
from SkyCast.forecast import ForecastWrapper

def index(request):
    return render(request, 'location_form.html')

def fetch_weather(request):

    if request.method == 'POST':
        location = str(request.POST.get('textfield', None))
        fw = ForecastWrapper(location)

        return render_to_response('index.html',
                                  {'summary': fw.get_summary(),
                                   'temp': fw.get_temperature_str(),
                                   'humidity': fw.get_humidity_str(),
                                   'precip': fw.get_precipitation_str(),
                                   'location': fw.get_location()}
                                  )
    else:
        return render(request, 'location_form.html')
